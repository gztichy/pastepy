#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from urllib import parse

VALIDKEYWORDS = {"key":"", "userKey":"", "mode":"paste", "private":"0",
                "name":"Untitled", "text":"asdf", "expire":"N", "type":"text"}

VALIDKEYNAMES = {"key":"dev_key", "userKey":"user_key", "mode":"option",
                "private":"paste_private", "name":"paste_name",
                "text":"paste_code", "expire":"paste_expire_date",
                "type":"paste_format"}

FILETYPES = {"py":"python", "c":"c", "h":"c", "cpp":"cpp","hpp":"cpp",
            "diff":"diff", "pl":"perl", "sh":"bash", "ini":"ini"}

class PastebinInfo(object):
    """A class that stores all Pastebin info necessary for using the API."""

    #####
    ##
    #  OVERLOADS
    ##
    #####

    def __init__(self, **kwargs):

        for key in VALIDKEYWORDS:

            if key in kwargs:
                setattr(self, key, kwargs[key])
            else:
                setattr(self, key, VALIDKEYWORDS[key])


    def __repr__(self):
        retList = []

        for key in sorted(VALIDKEYWORDS):
            retApp = "{}={}".format(key, repr(getattr(self, key) ) )

            retList.append(retApp)

        retStr = ", ".join(retList)

        ret = "{}({})".format(self.__class__.__name__, retStr)

        return ret

    def __str__(self):
        retList = []

        for key in sorted(VALIDKEYWORDS):
            retApp = "api_{}={}".format(VALIDKEYNAMES[key], parse.quote(str(getattr(self, key) ) ) )
            retList.append(retApp)

        ret = "&".join(retList)

        return ret

    #####
    ##
    # CLASS METHODS
    ##
    #####

    @classmethod
    def fromFile(cls, filePath, **kwargs):
        """\
Takes a file path and generates a PastebinInfo with the filename, its
text, and additional info provided"""

        name = os.path.basename(filePath)
        ext  = name.rpartition(".")[2]

        kwargs["name"] = name
        kwargs["text"] = open(filePath, "r").read()

        if ext in FILETYPES:
            kwargs["type"] = FILETYPES[ext]


        return cls(**kwargs)


    #####
    ##
    #  OBJECT METHODS
    ##
    #####


    #!!!!!!!!!!!!!!!!!!!!!!#
    #!  END PASTEBININFO  !#
    #!!!!!!!!!!!!!!!!!!!!!!#
