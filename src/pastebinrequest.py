#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from urllib import request
import re

PASTEBINURL = "http://pastebin.com/api/api_post.php"

class PastebinError(Exception): pass

class PastebinRequest(object):

    def __init__(self, pbInfo):
        self.pbInfo = pbInfo


    def getPage(self):
        pbUrl = request.urlopen(PASTEBINURL, str(self.pbInfo).encode(), 10)
        url = pbUrl.read().decode("utf-8")

        if "Bad API request" in url:
            raise PastebinError(url)

        return url
