#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import getpass, os
from urllib import parse, request
from .pastebinrequest import PastebinError

KEYDIR      = "~/.pastebinkeys"
KEYSEP      = "="
LOGINURL    = "http://pastebin.com/api/api_login.php"


def loginPastebin(apiKey=None, user=None, password=None, write=False):

    keys = getAPIKeys()

    if keys[1] is not None:    # already logged in
        return keys[1]

    if apiKey is None:
        if keys[0] is None:
            apiKey = input("API key: ")
        else:
            apiKey = keys[0]

    apiKey = parse.quote(apiKey)

    while not user:
        user = parse.quote(input("User: "))

    while not password:
        password = parse.quote(getpass.getpass("Password: "))

    post = "api_dev_key={}&api_user_name={}&api_user_password={}".format(apiKey, user, password)

    bPost = post.encode()

    pbUrl = request.urlopen(LOGINURL, bPost, 10)
    resp  = pbUrl.read()
    resp  = resp.decode("utf-8")

    if "Bad API request" in resp:
        raise PastebinError(resp)

    # since we've gotten this far, we can assume that the key isn't in KEYDIR
    if write:
        keyFile = open(replaceHome(KEYDIR), "a")
        keyFile.write("userkey = {}\n".format(resp))
        keyFile.close()

    return resp



def replaceHome(path):

    if path.startswith("~"):

        firstSep = path.find(os.sep)

        if firstSep == -1:
            user = path[1:]
        else:
            user = path[1:firstSep]

        if len(user) == 0:
            path = os.getenv("HOME") + path[1:]

        else:
            if firstSep == -1:
                path = "/home/" + user
            else:
                path = "/home/" + user + path[firstSep:]

    return path

def getKeys(path=KEYDIR, createFail=False):
    
    path = replaceHome(path)

    path = os.path.abspath(path)
    keys = {}
    
    try:
        keyFile = open(path, "r")
    except IOError as e:
        if createFail:
            open(path, "w")
            return {}
        else:
            raise IOError("{} doesn't exist".format(path)) from e

    for line in keyFile:

        keypairs = line.split(";")

        for keypair in keypairs:
            if KEYSEP not in keypair:
                continue

            lSplit  = keypair.split(KEYSEP, 1)

            kName   = lSplit[0].strip().lower()
            kValue  = lSplit[1].strip()

            keys[kName] = kValue

    return keys

def getAPIKeys(path=None):

    if path:
        keys = getKeys(path, True)
    else:
        keys = getKeys(createFail=True)

    return (keys.get("apikey"), keys.get("userkey"))
