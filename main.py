#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import sys
import os

from src import pastebinfuncs, pastebininfo, pastebinrequest

USAGEMESSAGE = "usage: {} <file>".format(os.path.basename(sys.argv[0]) )

expireChoices = ["10M", "1H", "1D", "1M", "N"]

def errorExit(message):
    print("ERROR: " + message, file=sys.stderr)
    sys.exit(1)

def main(args):

    streamMode = False

    if args.file is None:
        streamMode = True

    apiKey, userKey = pastebinfuncs.getAPIKeys()

    if apiKey is None:
        print("Adding API key to {}.".format(pastebinfuncs.KEYDIR) )
        print("You can get your API key at \"http://pastebin.com/api\".")
        while not apiKey:
            apiKey = input("API key: ")

        keyFile = open(pastebinfuncs.replaceHome(pastebinfuncs.KEYDIR), "a")
        keyFile.write("apikey = {}\n".format(apiKey))
        keyFile.close()

    if userKey is None:
        print("Adding user key to {}.".format(pastebinfuncs.KEYDIR) )
        userKey = pastebinfuncs.loginPastebin(apiKey, write=True)

    # by now, we should have a user and API key

    if streamMode:
        pbBuffer = []
        pbString = ""

        try:
            while 1:
                pbBuffer.append(input())
        
        except KeyboardInterrupt:
            print()
            return 1

        except EOFError:
            if pbBuffer:
                pbString = "\n".join(pbBuffer)
            else:
                return 0

        pbInfo = pastebininfo.PastebinInfo(text=pbString, name="<stdin>", key=apiKey, userKey=userKey)

    else:
        pbFile = args.file

        try:
            pbInfo = pastebininfo.PastebinInfo.fromFile(pbFile, key=apiKey, userKey=userKey)

        except IOError:
            errorExit("\"{}\" does not exist".format(pbFile) )
            return 1


    if args.name is not None:
        pbInfo.name = args.name

    if args.private:
        pbInfo.private = 2
    elif args.unlisted:
        pbInfo.private = 1
    else:
        pbInfo.private = 0

    pbInfo.private = str(pbInfo.private)

    args.expire = args.expire.upper()

    if args.expire not in expireChoices:
        errorExit("invalid expiration choice \"{}\"".format(args.expire))
    else:
        pbInfo.expire = args.expire

    #print(pbInfo)
    pbRequ = pastebinrequest.PastebinRequest(pbInfo)

    try:
        url = pbRequ.getPage()

    except pastebinrequest.PastebinError:
        exc = sys.exc_info()
        errorExit("{}".format(exc[1]))
        return

    print(url)

if __name__ == '__main__':
    #print(sys.argv)

    argp = argparse.ArgumentParser("pastebin")
    argp.add_argument("file", nargs="?", type=str, help="file to paste (optional)")
    argp.add_argument("-n", "--name", help="name of paste", type=str)
    argp.add_argument("-e", "--expire", help="when paste expires ({})".format(", ".join(expireChoices)), type=str, default=expireChoices[-1])
    argp.add_argument("-p", "--private", help="determines if paste is private", action='store_true')
    argp.add_argument("-u", "--unlisted", help="determines if paste listed", action='store_true')

    args = argp.parse_args()

    exit = main(args)

    if exit and isinstance(exit, int):
        sys.exit(exit)
